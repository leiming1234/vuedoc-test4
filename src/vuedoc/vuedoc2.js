const vuedoc = require('@vuedoc/md')
const options = {
  join: true,
  filenames: [
    '../components/HelloWorld.vue'
  ],
  labels: {
    author: '作者',
    'props.title': 'prop属性',
    'data.title': 'data属性',
    'computed.title': '计算属性',
    'method.title': '方法',
    'slots.title': '插槽slots',
    'props.name': '参数',
    'props.type': '类型',
    'props.required': '是否必须',
    'props.default': '默认值',
    'props.description': '说明'
  }
}

vuedoc.md(options)
  .then((document) => {
    console.log(document)
  })
  .catch((err) => console.error(err))
