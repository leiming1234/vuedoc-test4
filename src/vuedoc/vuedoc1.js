const vuedoc = require('@vuedoc/md')
const options = {
  join: true,
  filenames: [
    '../components/HelloWorld.vue'
  ]
}

vuedoc.md(options)
  .then((document) => {
    console.log(document)
  })
  .catch((err) => console.error(err))
