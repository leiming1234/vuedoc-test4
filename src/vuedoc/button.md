# VButton

VButton

## prop属性

| 参数         | 类型        | 说明                | 默认值         |
| ---------- | --------- | ----------------- | ----------- |
| `kind`     | `String`  |                   |             |
| `status`   | `String`  | 按钮的样式类型           | `"default"` |
| `size`     | `String`  | 按钮的大小             | `"medium"`  |
| `type`     | `String`  |                   | `"button"`  |
| `loading`  | `Boolean` | 是否处于loading状态     | `false`     |
| `disabled` | `Boolean` | 是否处于disabled状态    | `false`     |
| `href`     | `String`  | 按钮实际为a标签，其地址      | `""`        |
| `target`   | `String`  | 按钮实际为a标签，其target值 | `"_self"`   |
| `icon`     | `String`  | 按钮内嵌图标名称          | `""`        |

