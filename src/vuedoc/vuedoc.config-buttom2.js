module.exports = {
  output: 'src/vuedoc/',
  // join: true,
  parsing: {
    jsx: true
  },
  filenames: [
    'src/components/button/index.vue'
  ],
  labels: {
    author: '作者',
    'props.title': 'prop属性',
    'data.title': 'data属性',
    'computed.title': '计算属性',
    'method.title': '方法',
    'slots.title': '插槽slots',
    'props.name': '参数',
    'props.type': '类型',
    'props.required': '是否必须',
    'props.default': '默认值',
    'props.description': '说明'
  }
}
